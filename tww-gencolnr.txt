1

Twin-width and generalized coloring numbers

2

Jan Dreier

3

#
Vienna University of Technology

4

Jakub Gajarský

5

University of Warsaw

6

Yiting Jiang

#

8

#
Université de Paris, CNRS, IRIF, F-75006, Paris, France and Department of Mathematics, Zhejiang
Normal University, China

9

Patrice Ossona de Mendez

7

11

#
Centre d’Analyse et de Mathématiques Sociales (CNRS, UMR 8557), Paris, France
Computer Science Institute of Charles University, Praha, Czech Republic

12

Jean-Florent Raymond

10

13

#
CNRS, LIMOS, Université Clermont Auvergne, France

Abstract

14

17

In this paper, we prove that a graph G with no Ks,s -subgraph and twin-width d has r-admissibility
and r-coloring numbers bounded from above by an exponential function of r and that we can
construct graphs achieving such a dependency in r.

18

2012 ACM Subject Classification Mathematics of computing → Graph theory

19

Keywords and phrases Twin-width, generalized coloring numbers

15
16

20
21
22
23
24
25
26
27

Funding Patrice Ossona de Mendez: This paper is part of a project that has received funding from
the European Research Council (ERC) under the European Union’s Horizon 2020 research and
innovation programme (grant agreement No 810115 – Dynasnet).
Jakub Gajarský: The research reported in this paper is supported by the European Research
Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (ERC
consolidator grant LIPA, agreement No. 683080).
Yiting Jiang: This work is partially supported by the ANR project HOSIGRA (ANR-17-CE400022)

28

29

30
31
32
33
34
35
36
37
38
39
40
41

1

Introduction

In this paper we consider the twin-width graph parameter, defined by Bonnet, Kim, Thomassé
and Watrigant [5] as a generalization of a width invariant for classes of permutations
defined by Guillemot and Marx [10]. This parameter was intensively studied recently in the
context of many structural and algorithmic questions such as FPT model checking [5], graph
enumeration [1], graph coloring [3], matrices and ordered graphs [4], and transductions of
permutations [2]. (We postpone the formal definition of twin-width to Section 2.1.)
It is known that a graph class with bounded twin-width excludes some biclique as a
subgraph if and only if it has bounded expansion [1]. Recall that a class C has bounded
expansion if, for each integer r the class of all the minors of graphs of C obtained by
contracting vertex disjoint connected subgraphs with radius at most r and deleting some
edges and vertices have bounded average degree, which may depend on r. (We refer the
interested reader to [12] for an in-depth study of classes with bounded expansion.) Among the

2

Twin-width and generalized coloring numbers

42
43
44
45
46
47
48
49
50
51

52
53
54
55
56
57
58
59
60

numerous characterizations of classes with bounded expansion, three relate to the generalized
colouring numbers wcolr and scolr introduced by Kierstead and Yang [11] and to the radmissibility admr introduced by Dvořák [7]. Indeed, as proved by Zhu [13], the following
are equivalent for a class C :
1. C has bounded expansion;
2. sup{wcolr (G) : G ∈ C } < ∞ for every integer r;
3. sup{scolr (G) : G ∈ C } < ∞ for every integer r.
r+1
−1
r (G)
Moreover, using the inequality admr (G) ≤ scolr (G) ≤ wcolr (G) ≤ adm
(see [7]), we
admr (G)−1
get yet another equivalent property.
4. sup{admr (G) : G ∈ C } < ∞ for every integer r.
One can show [1] that for every integer r there exists a function fr : N × N → N such that
if G is a graph with twin-width t and no Ks,s -subgraph, then we have wcolr (G) ≤ fr (t, s).
Similar bounds also exist for scolr and admr . However, the proof given in [1] that biclique-free
graphs with bounded twin-width have bounded expansion does not indicate how to compute
such binding functions.
In this paper, we prove that a graph G with no Ks,s -subgraph and twin-width d has admr ,
scolr and wcolr bounded from above by an exponential function of r, and that we can construct
graphs achieving such a dependency in r. In particular, scolr (G) ≤ (dr + 3)s (Theorem 2).
r
On the other hand, one can choose G such that scolr (G) ≥ ( d−4
8 ) s (Corollary 13).

61

2

62

2.1

63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86

Definitions and Notations
Twin-width

We define twin-width with the help of trigraphs. The notion of trigraphs used in this work
is slightly different from the notion used in [5]. Both notions are nevertheless equivalent
up to isomorphism. A trigraph G on a graph G = (V, E) is a binary structure with two
binary relations, the black adjacency E and the red adjacency R, whose domain is a partition
of V , and whose black and red adjacencies are exclusive (that is: no two elements of G
can be adjacent in both relations). Thus, the elements of G are subsets of the vertices of
G. To distinguish the elements of G from the vertices of G, we will call them nodes and
denote them by capital letters, like X, Y, Z. The set of nodes of G is denoted by V (G). The
elements of E(G) and R(G) are respectively called black edges and red edges. The set of
neighbours NG (X) of a node X in a trigraph G consists of all the nodes adjacent to X by a
E
black or red edge; the set of E-neighbours NG
(X) consists of all nodes adjacent to X by a
R
black edge and the set of R-neighbours NG (X) consists of all nodes adjacent to X by a red
R
edge. A d-trigraph is a trigraph G with maximum red degree at most d, i.e., |NG
(X)| ≤ d
for all X ∈ V (G).
Let G be a trigraph on a graph G and let X and Y be (non-necessarily adjacent) nodes
of G. We say a trigraph G′ on G is obtained from G by contracting X and Y if V (G′ ) =
E
E
E
V (G) \ {X, Y } ∪ {X ∪ Y }, NG′ (X ∪ Y ) = NG (X) ∪ NG (Y ), NG
′ (X ∪ Y ) = NG (X) ∩ NG (Y )
R
E
(and NG′ (X ∪ Y ) = NG′ (X ∪ Y ) \ NG′ (X ∪ Y )), and the red and black adjacencies between
all other nodes of G′ are as in G.
A d-contraction sequence of a graph G = (V, E) with n vertices is a sequence Gn , . . . , G1
of d-trigraphs on G, where Gn is the trigraph isomorphic to G defined by V (Gn ) = {{v} :
v ∈ V }, E(Gn ) = {({u}, {v}) : (u, v) ∈ E(G)}, and R(Gn ) = ∅, G1 is the trigraph with
single node V , and Gi is obtained from Gi+1 by performing a single contraction. The
minimum d such that there exists a d-contraction sequence of a graph G is the twin-width

J. Dreier, J. Gajarsky, Y. Jiang, P. Ossona de Mendez, and J.-F. Raymond

87
88
89
90
91
92
93
94
95
96
97
98
99

100

101
102
103
104
105
106
107
108
109
110
111
112
113
114
115

of G, denoted by tww(G). For a contraction sequence Gn , . . . , G1 , we define the universe
Sn
U = i=1 V (Gi ) to be the union of all node sets.
A given contraction sequence Gn , . . . , G1 on a graph G = (V, E) (with universe U) can
also be reversed to G1 , . . . , Gn and seen as an uncontraction sequence where we start with
a single node (the trigraph G1 ) and a node Z of Gi is split into two nodes X and Y with
no edge, black edge or red edge between them in Gi+1 . With this picture in mind, we
define for every X ∈ U, the birth time bt(X) as the minimum integer i with X ∈ V (Gi )
and the split time st(X) as the maximum integer i with X ∈ V (Gi ). Observe that for
every i ∈ {1, . . . , n − 1}, there is a unique X ∈ U with st(X) = i; the subsets X ∈ U with
st(X) = n are the nodes of Gn , that is the singletons {v} with v ∈ V (G). If X ∈ U \ {V },
the parent of X is the minimal set Y ∈ U with Y ⊋ X. Conversely, if |X| > 1, the children
of X are the two maximal sets Y and Z in U with Y ⊊ X and Z ⊊ X. Note that {Y, Z} is
a partition of X and that bt(Y ) = bt(Z) = st(X) + 1.

2.2

Generalized Colouring Numbers and Admissibility

Let Π(G) be the set of all linear orders of the vertices of the graph G, and let L ∈ Π(G).
(We denote by ≤L the corresponding binary relation for better readability.) Let u, v ∈ V (G),
and let r be a positive integer.
We say that u is weakly r-reachable from v with respect to L, if there exists a path
P of length at most r between u and v such that u ≤L w for all vertices w of P . Let
WReachr [L, v] be the set of vertices that are weakly r-reachable from v with respect to L.
Note that v ∈ WReachr [L, v].
We say that u is strongly r-reachable from v with respect to L, if there is a path P of
length at most r connecting u and v such that u ≤L v and all inner vertices w of P satisfy
v <L w. Let SReachr [L, v] be the set of vertices that are strongly r-reachable from v with
respect to L. Note that again we have v ∈ SReachr [L, v].
The r-backconnectivity br (L, v) of a vertex v is the maximum number of paths of length
at most r in G that start in v, share no other vertices except v, and end at vertices that lie
before v in the ordering L.
The weak r-colouring number wcolr (G) of G is defined as
wcolr (G) := min

116

max

L∈Π(G) v∈V (G)

117

WReachr [L, v] ,

and the strong r-colouring number scolr (G) of G is defined as
scolr (G) := min

118

max

L∈Π(G) v∈V (G)

SReachr [L, v] ,

119

The r-admissibility of G is defined as

120

admr (G) = min

max br (L, v).

L∈Π(G) v∈V (G)

121

122
123
124
125

3

From Strong Colouring to Weak Colouring

It is known that the weak and strong colouring numbers are related by scolr (G) ≤ wcolr (G) ≤
scolr (G)r [11]. However, it is possible to improve the upper bound in the case where some
linear order witnesses some upper bound for strong coloring numbers that increase at least
at an exponential rate.

3

4

Twin-width and generalized coloring numbers

126

▶ Lemma 1. For every graph G and every positive integer r we have
wcolr (G) ≤ 2r−1 min

127

max

max SReachk [L, v]

r/k

L∈Π(G) v∈V (G) 1≤k≤r

128
129
130
131
132
133
134
135
136
137
138

139

Proof. Let r be a positive integer and let L be a linear order on V (G). Let u be a vertex of
G, v ∈ WReachr [L, u], and consider a path P certifying that v is weakly r-reachable from
u; in particular P has length at most r. Let C(r) be the set of all compositions of r, that
P
is of all tuples (r1 , . . . , rk ) with ri > 0 (for 1 ≤ i ≤ k) and 1≤i≤k ri = r. A milestone of
P is a vertex v of P such that all the vertices of P from u (included) to v (excluded) are
greater than v. Let v1 , . . . , vk = v be the milestones of P other than u, and let r1 , . . . , rk−1
Pk−1
be the lengths of the paths from v0 = u to v1 ,. . . , vk−2 to vk−1 , and let rk = r − i=1 ri ,
so that (r1 , . . . , rk ) ∈ C(r). The subpath of P from vi−1 to vi witnesses that vi is strongly
ri -reachable from vi−1 . Note that strong rk -reachability requires the existence of a witness
path of length at most rk , hence it is safe to consider rk instead of the length of the subpath
of P linking vk−1 and vk . We deduce that
[
[
[
WReachr [L, u] ⊆
···
SReachrk [L, vk−1 ].
(r1 ,...,rk )∈C(r)

140
141

X

wcolr (G) ≤

k
Y

(r1 ,...,rk )∈C(r) i=1

144

v1 ∈SReachr1 [L,u]

vk−1 ∈SReachrk−1 [L,vk−2 ]

(Note that we actually have equality, the reverse inclusion following from the concatenation
of paths witnessing v1 ∈ SReachr1 [L, v], . . . , u ∈ SReachrk [L, vk−1 ].) Thus we have

142

143

.

max
v∈V (G)


SReachri [L, v] .

Let z = max1≤k≤r maxv∈V (G) SReachk [L, v]
Thus
X

wcolr (G) ≤

145

k
Y

1/k

. Then maxv∈V (G) SReachri [L, v] ≤ z ri .

z ri = |C(r)| z r = 2r−1 z r .

(r1 ,...,rk )∈C(r) i=1

◀

146

147

4

Upper bounds

148

Let bω(G) denote the maximum integer s such that Ks,s is a subgraph of G.

149

▶ Theorem 2. For every graph G and every positive integer r we have


150

scolr (G) ≤

3 + tww(G)


r−1
X
(tww(G) − 1)i bω(G) ≤ (tww(G)r + 3) bω(G).

(1)

i=0
151
152

153
154
155
156
157
158

Moreover, for any fixed graph G, all these inequalities (for all possible values of r) are
witnessed by a single linear order on V (G).
Proof. Let d = tww(G) and s = bω(G). Without loss of generality, we can assume that G
is connected and contains more than s vertices. We consider a d-uncontraction sequence
G1 , . . . , Gn of G with universe U. For every i ∈ {1, . . . , n} we say a node of Gi is small if
it contains at most s vertices and it is big, otherwise. A set X ∈ U is nice at step i with
bt(X) ≤ i ≤ st(X) if X is small and some black edge is incident to it in Gi . Note that if X
is nice at step i, it is nice at step j for all i ≤ j ≤ st(X). The set X is nice if it is nice at

J. Dreier, J. Gajarsky, Y. Jiang, P. Ossona de Mendez, and J.-F. Raymond

159
160
161
162
163
164
165
166
167
168
169

170

171
172

173

174
175
176

177

178
179
180

some step (equivalently, at step st(X)). For every nice set X we define ρ(X) as the minimum
i such that X is nice at step i. (Note that ρ(X) > 1 as G1 is edgeless.) As G is connected,
it is clear that every X ∈ U has a subset Y ∈ U that is nice. Also, if X, Y ∈ U, X ⊆ Y and
Y is nice, then X is also nice. It follows that the family N of all the maximal nice sets in U
form a partition of V . We order the elements of N as N1 , . . . , Nk in such a way that for all
i < j, either ρ(Ni ) < ρ(Nj ) holds or ρ(Ni ) = ρ(Nj ) and bt(Ni ) ≥ bt(Nj ). We now fix any
linear ordering L of V such that for all v ∈ Ni , w ∈ Nj with i < j holds v <L w. See also
Remark 6 for an equivalent algorithmic way to define the order L. We will use this ordering
to bound the strong coloring numbers of G.
For 1 ≤ i ≤ n, we define Bi to be the set of all nodes of Gi that are not nice at step i.
We first establish some easy properties of Bi .
▷ Claim 3. No small node in Bi is incident to a black edge in Gi .
Proof of the claim. Assume X ∈ Bi is small. Then it is not adjacent to a black edge as it is
not nice at step i.
◁
▷ Claim 4. No two nodes in Bi are adjacent in Gi by a black edge.
Proof of the claim. Assume for contradiction that X and Y are nodes in Bi that are adjacent
by a black edge in Gi . Hence, G[X ∪ Y ] includes K|X|,|Y | as a subgraph. According to
Claim 3, both X and Y are big, contradicting the assumption bω(G) = s.
◁
S E
▷ Claim 5. Every node X ∈ Bi is such that | NG
(X)| ≤ s.
i
S E
Proof of the claim. Let X ∈ Bi and let Y = NG
(X). Then X and Y induce a biclique in
i
G thus min(|X|, |Y |) ≤ s. As only big nodes in Bi are adjacent to black edges (by Claim 3),
we deduce |X| > s and therefore |Y | ≤ s.
◁

186

Let us consider a vertex v ∈ V . In the remainder of the proof, we will bound the number
of vertices in G that are strongly r-reachable from v with respect to L. Let a ∈ {1, . . . , k}
be such that v ∈ Na and let t = ρ(Na ) − 1. Let S be the unique node of Gt with st(S) = t,
and let Y, Z be the two children of S.
Let L = {X ∈ V (Gt ) : (∃i < a), Ni ⊇ X}. By definition of L, all the vertices of G that
belong to the nodes in L appear before v in L. If we set R = V (Gt ) \ L, then R ⊆ Bt .

187

Case 1: v ∈ S.

181
182
183
184
185

188
189
190
191
192
193
194
195
196
197
198
199
200

Note that if a vertex u ∈ V (G) is strongly r-reachable from v, then u belongs either to S or
to a set in L. We consider a BFS-tree T in Gt , starting at S, following only red edges, with
depth r, and stopping each time it reaches a node in L. Note that, by construction, T has
no internal node in L, but it may have leaves that are not in L (e.g. nodes at depth r). We
further remove from T any node with no descendant (in T ) belonging to L. This way we get
a tree T rooted at S, with internal nodes in R, with depth at most r, and with leaves in
L. Let I denote the sets of all internal nodes of T and let E be the sets of all leaves of T .
Pr−2
Then |I| ≤ 1 + ℓ=0 d(d − 1)ℓ and |E| ≤ d(d − 1)r−1 . Consider any vertex u that is strongly
r-reachable from v, and let P be a path from v to u in G witnessing this. We can project
P onto Gt by mapping every vertex to the node of Gt containing it. The projection is a
walk from S to a node Xu containing u. From this walk we extract a path Q of length at
most r from S to Xu . All the internal nodes of Q as well as S belong to R, hence all the
edges of Q (but maybe the last one) are red (according to Claim 4). Moreover, Xu is either

5

6

Twin-width and generalized coloring numbers

201
202
203
204
205
206
207
208

209

210

S or it belongs to L. So, either u ∈ S, or Xu has been reached by a black edge from some
internal node of T , or Xu is a leaf of T . It is easily checked that at most |I|s vertices of G
can be of the second type (according to Claim 5), and at most |E|s are of the last type (as
leaves belong to L, so they are small). Regarding the first type, we assume without loss of
generality that v ∈ Z and observe that either u ∈ Z, so there are at most s choices for u (as
Z = Na is nice hence small at time t + 1), or u ∈ Y but then, as u ≤L v, Y is nice as well at
time t + 1 so |Y | ≤ s. Thus at most 2s vertices of G can be of the first type. Altogether, we
get

| SReach[G, L, v]| ≤ 2 + 1 + d + · · · + d(d − 1)r−2 + d(d − 1)r−1 s
!
r−1
X
ℓ
≤ 3+d
(d − 1) s.
ℓ=0

211

212
213
214
215
216
217
218
219
220
221
222
223
224
225
226
227
228

229

230

Case 2: v ∈
/ S.
Note that if u is strongly r-reachable from v, then u belongs either to S or to Na , or to a set
in L. We consider a BFS-tree T in Gt , starting at Na , following only red edges, with depth r,
and stopping each time it reaches a node in L. We further remove from T any node with no
descendant (in T ) belonging to L ∪ {S}. This way we get a tree T rooted at Na , with internal
nodes in R, with depth at most r, and with leaves in L. Let I denote the sets of all internal
nodes of T and let E be the sets of all leaves of T . Then |I| ≤ 1 + d + · · · + d(d − 1)r−2 and
|E| ≤ d(d − 1)r−1 . Consider any vertex u that is strongly r-reachable from v, and let P be a
path from v to u witnessing this. The path P projects on Gt as a walk with length at most
r from Na to the vertex Xu containing u. From this walk we extract a path Q with length
at most r from Na to Xu . All the internal nodes of Q belong to R hence all the edges of Q
(but maybe the last one) are red (according to Claim 4). Moreover, Xu is either Na , or S, or
it belongs to L. So, either u ∈ Na , or u ∈ S, or Xu has been reached by a black edge from
some internal node of T , or Xu is a leaf of T . The first type correspond to at most s vertices.
The second type correspond to at most 2s vertices because in this case, Y , Z, or both, have
been ordered by L before Na which mean they are nice at step t + 1, hence small. The third
type correspond to at most (|I| − 1)s, as the root Na is small hence adjacent to no black
edge. The last type correspond to at most |E|s vertices. Altogether, we get

| SReach[G, L, v]| ≤ 1 + 2 + (1 + d + · · · + d(d − 1)r−2 − 1) + d(d − 1)r−1 s
!
r−1
X
ℓ
≤ 3+d
(d − 1) s.
ℓ=0

231
232

233
234
235
236
237
238
239
240
241

Thus in both cases we have that every graph G with tww(G) = d and bω(G) = s
satisfies (1).
◀
▶ Remark 6. We describe an algorithmic procedure that also yields the order L defined
in Theorem 2. We are given an uncontraction sequence Gn , . . . , G1 . For each i ∈ {1, . . . , n−1}
we define a function origini : V (Gi+1 ) → V (Gi ) that, informally, assigns each node in Gi+1
to the node in Gi that it originates from. Let us be more precise: assume Gi+1 is constructed
from Gi by splitting a node Z into two nodes X and Y , then origini (X) = origini (Y ) = Z
and origini (W ) = W for every other node W of Gi+1 .
Remember that a node of Gi is small if it contains at most s vertices and is big, otherwise.
A node X ∈ V (Gi ) is a nice node of Gi if X is small and some black edge is incident to X
in Gi . We incrementally construct for all i an ordering <i on the nice nodes of Gi . Since

J. Dreier, J. Gajarsky, Y. Jiang, P. Ossona de Mendez, and J.-F. Raymond

242
243
244
245
246
247
248
249
250
251
252
253
254
255
256
257
258

259

260

261

all nodes of Gn are nice and correspond to singletons, the ordering <n then corresponds to
an ordering of the vertices of G. This order will be equivalent to the ordering L defined in
Theorem 2 (up to non-determinism).
Since G1 has no nice nodes, <1 is the ordering on the empty set. Assuming that <i is
already constructed, we construct <i+1 such that it satisfies for all nice X, Y ∈ V (Gi+1 ) the
following conditions.
1. If origini (X) and origini (Y ) are nice in Gi and origini (X) <i origini (Y ) then X <i+1 Y .
2. If origini (X) is nice in Gi and origini (Y ) is not nice in Gi then X <i+1 Y .
3. If both origini (X) and origini (Y ) are not nice in Gi and bt(X) > bt(Y ) then X ≤i+1 Y .
Each order <i represents a partial order on V that is refined over time as i increases, until
we reach a total order on V . Rule 1. states that the old order is preserved when possible,
rule 2. states that new nice sets are appended at the end and rule 3. makes sure that we
append new nice sets in order of their birth.
In the proof of Theorem 2, we fix a vertex v ∈ V and pick t maximal such that in Gt the
node N containing v is not nice. We then partition the nodes of Gt into sets L and R. One
can show that L contains precisely those nodes of Gt that are strictly smaller than N with
respect to <t .
▶ Corollary 7. For every graph G and every positive integer r we have


2 bω(G)



3 bω(G)
scolr (G) ≤

5 bω(G)




3(tww(G) − 1)r bω(G)

if tww(G) = 1,
if tww(G) = 2,
if tww(G) ≥ 3.

Proof. If tww(G) ≥ 3 we have


262

if tww(G) = 0,

scolr (G) ≤

3 + tww(G)

(tww(G) − 1)r − 1
tww(G) − 2



263

≤ (3 + 3((tww(G) − 1)r − 1)) bω(G)

264

≤ 3(tww(G) − 1)r bω(G).

265
266
267
268
269
270
271
272
273
274
275
276
277

278

bω(G)

The cases where tww(G) = 1 or 2 follow from the theorem. If tww(G) = 0 then G is a
cograph. Let us then show that for every cograph G it holds that scolr (G) ≤ 2 bω(G).
The proof is by induction on the number of vertices. The base case |V (G)| = 1 is trivial
so we consider a cograph with at least two vertices and assume that the desired bound holds
for all cographs on less vertices. Being a cograph, G can be obtained from two cographs
G1 and G2 by disjoint union or complete join [6]. Without loss of generality we assume
|V (G1 )| ≤ |V (G2 )|. By induction, for every i ∈ {1, 2} there is an ordering Li of V (Gi ) such
that scolr (Gi ) ≤ 2 bω(Gi ).
Then the order L is obtained by putting first L1 , then L2 . If G is the disjoint union of
G1 and G2 then scolr (G, L) = max(scolr (G, L1 ), scolr (G, L2 )) and the result follows; if G is
the complete join of G1 and G2 then scolr (G, L) ≤ scolr (G2 , L2 ) + |V (G1 )| and bω(G) ≥
bω(G2 ) + |V (G1 )|/2. As scolr (G2 , L2 ) ≤ 2 bω(G2 ), we get scolr (G2 , L2 ) ≤ 2 bω(G) − |V (G1 )|
hence scolr (G, L) ≤ 2 bω(G).
◀
Combining Lemma 1 with Theorem 2 we get the following.

7

8

Twin-width and generalized coloring numbers

279

▶ Corollary 8. For every graph G and every positive integer r we have
wcolr (G) ≤

280

281
282
283

284
285

286
287
288
289
290
291
292

293

Note that the base of the exponential comes from the degeneracy of G. In order to improve
this upper bound it is thus natural to try to improve the degeneracy bound. Hence the
following problem:
▶ Problem 9. What is the maximum degeneracy of a Ks+1,s+1 -free graph with twin-width at
most d?
Recall that a depth r minor of a graph G is a graph H obtained from G by taking a
subgraph and contracting vertex disjoint subgraphs of radius at most r. The greatest reduced
average density (grad) of G with rank r is the maximum ratio |E(H)|/|V (H)| over all (nonempty) depth r minors of a graph G; it is denoted by ∇r (G). Hence, by definition, a class C
has bounded expansion if, for each positive integer r, we have sup{∇r (G) : G ∈ C } < ∞.
It is known that ∇r (G) ≤ wcol2r+1 (G) [13]. Hence the next corollary directly follows from
Corollary 8.
▶ Corollary 10. For every graph G and every positive integer r we have
∇r (G) ≤

294

295
296

297

298
299
300
301

302
303

304

305
306

307

308
309
310
311
312
313

r
1
(2 tww(G) + 6) bω(G) .
2

1
((2 tww(G) + 6) bω(G))2r+1 .
2

(2)

In particular, every class of graphs of bounded clique-width that exclude a biclique as a
subgraph has (at most) exponential expansion.

5

Lower bounds

It is known that high-girth graphs have large strong coloring numbers [9]. On the other
hand, there exist expander graphs with high girth and small twinwidth [1]. We combine
both results to construct graphs with small twinwidth whose strong r-coloring numbers grow
exponentially in r.
▶ Proposition 11 ([9, Theorem 5.1]). Let G be a d-regular graph of girth at least 4g + 1,
where d ≥ 7. Then for every r ≤ g,

2⌊log2 r⌋ −1
d d−2
scolr (G) ≥
.
2
4
▶ Lemma 12. For every integer ∆ ≥ 7 and every integers r and g ≥ 4r + 1 there exists a
∆-regular graph G with girth at least g, 2∆ − 1 ≤ tww(G) ≤ 2∆, and
scolr (G)) ≥


2⌊log2 r⌋ −1

2⌊log2 r⌋ −1
∆ ∆−2
tww(G) tww(G) − 4
≥
.
2
4
4
8

Proof. We will construct a sequence G0 , G1 , . . . of ∆-regular graphs of twin-width at most
2∆ and increasing girth. Once we reach a graph with girth at least g ≥ 4r + 1, the result of
this lemma follows from Proposition 11. Note that the twin-width of a ∆-regular graph with
girth at least 5 is at least 2∆ − 1 (because of the first contraction).
We define G0 to be the complete graph with ∆ + 1 vertices. We fix a graph Gk−1
with edges e1 , . . . , em and describe how to construct Gk . For every edge ei of Gk−1 , we

J. Dreier, J. Gajarsky, Y. Jiang, P. Ossona de Mendez, and J.-F. Raymond

314
315

316

317
318
319
320
321
322
323
324
325
326
327
328
329
330
331
332
333

334
335

336

337
338

339
340
341
342
343
344

345
346
347

348
349

350
351

352
353

define a mapping θei : {0, 1}m → {0, 1}m that flips the ith coordinate and preserves all other
coordinates, i.e., θei ((x1 , . . . , xm )) = (y1 , . . . , ym ) with
(
1 − xj if j = i
yj =
xj
otherwise.
We define Gk to be the graph with V (Gk ) = V (Gk−1 )×{0, 1}m and E(Gk ) = {{(u, x), (v, θuv (x)} :
uv ∈ E(Gk−1 ) and x ∈ {0, 1}m }.
A 2-lift of a graph G is a graph obtained by adding for every vertex v of G two vertices
v1 and v2 , and adding for every edge uv of G either the edges u1 v1 and u2 v2 (parallel edges)
or the edges u1 v2 and u2 v1 (crossing edges). The graph Gk+1 can be obtained by a sequence
of 2-lifts from Gk and therefore also by a sequence of 2-lifts from G0 = K∆+1 . We can
construct a contraction sequence that “undoes“ these 2-lifts by repeatedly contracting all
pairs of duplicates. Once we reach K∆+1 , we simply contract the remaining vertices one
by one. While doing so, the red degree never exceeds 2∆ (see also [1, Lemma 26]). Hence
tww(Gk ) ≤ 2∆.
It remains to show that the girth of Gk is higher than the girth of Gk−1 . Let γ be
a shortest cycle of Gk . Let pV : V (Gk ) → V (Gk−1 ) be the standard projection, and let
pE : E(Gk ) → E(Gk−1 ) be the associated projection. It is easily checked that applying pE
to a cyclic graph yields a cyclic graph, and thus pE (γ) includes a cycle. If we apply the
composition of all the mappings θpE (e) for e ∈ γ then the starting vertex of γ is fixed. It
follows that each θe is applied an even number of times. Thus the length of γ is at least
twice the length of pE (γ). Hence, the girth of Gk is at least twice the girth of Gk−1 .
◀
▶ Corollary 13. For every integer d ≥ 14, every positive integer s, and every integer r of the
form 2k , there exists a graph G with tww(G) ≤ d, bω(G) = s, and

r−1

r
ds d − 4
tww(G) − 4
scolr (G) ≥
≥2
bω(G).
4
8
8
Proof. Take the lexicographic product of a graph obtained by Lemma 12 and Ks . This way
we get a graph with twin-width at most d ≥ 14 and no Ks+1,s+1 .
◀
▶ Remark 14. The 2-lift construction used in the proof of Lemma 12 was used in [1] to prove
that there exist cubic expander graphs with twin-width at most 6. It follows from this result
and the characterization of classes with polynomial expansion [8] that for d ≥ 6, the value
∇r (G) is not bounded on the K2,2 -free graphs with twin-width at most d by a polynomial
function of r. We leave as a question wether sup{∇r (G) : tww(G) ≤ d and bω(G) ≤ s}
increases exponentially with r for sufficiently large d.
Admissibility being a lower bound for strong coloring numbers, the above results do not
provide any lower bound for admissibility. We show below how to construct classes of graphs
that have no K2,2 -subgraph with low twin-width and high admissibility.
▶ Lemma 15 ([1, Proposition 28]). For d ≥ 0 and k > 0, if the clique Kn subdivided k times
has twin-width less than d, then k ≥ logd (n − 1) − 1.
▶ Lemma 16 ([1, Proposition 31]). For any c > 0, the class of cliques Kn subdivided at least
log n
times has twin-width at most f (c) for some triple exponential function f .
c
▶ Lemma 17. For every integers d and r ≥ 4 there is a graph G such that
G has no K2,2 subgraph;

9

10

Twin-width and generalized coloring numbers

354
355
356

357
358
359

360
361
362
363

364

365

366

367
368
369
370
371
372
373
374
375

376
377

378

379
380
381
382
383
384
385
386
387
388

389

390
391
392
393
394

d ≤ tww(G) ≤ f (2 log d);
admr (G) ≥ d2(r−1) ,
where f is the function of Lemma 16.
In particular, the above lemma implies that for every d, there is a graph class of bounded
twin-width, whose members contain no K2,2 , but which has r-admissibility (and thus r-weak
and strong coloring numbers) at least d2(r−1) .
Proof. Let d, r ∈ N and n = d2(r−1) . We define Gdr as the graph obtained by subdividing
r − 1 times each edge of Kn . By construction, Gdr has no K2,2 subgraph. Let c = 2 log d and
notice that r − 1 = logc n . According to Lemma 16, Gdr has twin-width at most f (c). On the
other hand, as r > 3 we have
r − 1 < 2(r − 1) − 2


≤ logd d2(r−1) − 2
< logd (n − 1) − 1,
and therefore Lemma 15 implies that d ≤ tww(G).
In order to prove the bound on admissibility, let us now consider an arbitrary ordering
σ of V (Gdr ). Notice that Gdr has two types of vertices: n vertices of degree n − 1, which
correspond to the vertices of the n-clique that was used to construct Gdr , and vertices of
degree 2, which have been introduced by subdivisions. Let x denote the vertex of degree n − 1
that appears the latest in σ. Notice that there are n − 1 paths of length r that start in x, are
otherwise disjoint and end at the n − 1 other vertices of degree n − 1 of G. By definition of x,
these n − 1 vertices appear before x in the ordering. This implies admr (Gdr , σ) ≥ n = d2(r−1) .
As σ was chosen arbitrarily, the same bound holds for the r-admissibility of Gdr .
◀
▶ Corollary 18. For all integers d, r ≥ 4, there is a constant ε > 0 such that for all positive
integers r and n there is a Ks+1,s+1 -free graph G with |V (G)| ≥ n, bω(G) = s, and
admr (G) ≥ (log log tww(G))ε bω(G).
Proof. We first consider the case where s = 1. Let G0 be the graph given by Lemma 17.
If |V (G0 )| ≥ n then G0 is the desired graph. Otherwise, we denote by G the disjoint
union of n copies of G0 . Clearly this does not create any K2,2 thus bω(G0 ) = 1. We have
admr (G) ≥ d2(r−1) , as otherwise any ordering of G with smaller r-admissibility would give an
ordering with smaller r-admissibility for G0 . Finally, as the twin-width of the disjoint union
of two graphs is the maximum of the twin-width of each of them, we have tww(G) = tww(G0 ),
so tww(G) ≤ f (2 log d). The existence of the constant ε > 0 then follows from the fact that
f is a triple exponential function.
The case where s > 1 follows by considering the lexicographic product of the graphs
obtained above by Ks .
◀

Acknowledgments
These results have been obtained at the occasion of the workshop “Generalized coloring
numbers and friends” of the Sparse Graphs Coalition, which was co-organized by Michał
Pilipczuk and Piotr Micek.
The authors would like to thank Daqing Yang for pointing out an error in the statement
and proof of Lemma 1 in an earlier version of this paper.

J. Dreier, J. Gajarsky, Y. Jiang, P. Ossona de Mendez, and J.-F. Raymond

References

395

396

1

397
398
399

2

400
401

3

402
403

4

404
405

5

406
407
408

6

409
410

7

411
412

8

413
414

9

415
416

10

417
418
419

11

420
421

12

422
423
424

13

E. Bonnet, C. Geniet, E.J. Kim, S. Thomassé, and R. Watrigant, Twin-width II: small classes,
Proceedings of the 2021 ACM-SIAM Symposium on Discrete Algorithms (SODA), 2021,
pp. 1977–1996.
E. Bonnet, J. Nešetřil, P. Ossona de Mendez, and S. Siebertz, Twin-width and permutations,
arXiv:2102.06880 [cs.LO], 2021.
Édouard Bonnet, Colin Geniet, Eun Jung Kim, Stéphan Thomassé, and Rémi Watrigant,
Twin-width III: Max Independent Set and Coloring, arXiv:2007.14161 [cs.DS], 2020.
Édouard Bonnet, Ugo Giocanti, Patrice Ossona de Mendez, and Stéphan Thomassé, Twin-width
IV: low complexity matrices, arXiv:2102.03117 [math.CO], 2021.
Édouard Bonnet, Eun Jung Kim, Stéphan Thomassé, and Rémi Watrigant, Twin-width I:
tractable FO model checking, 61st IEEE Annual Symposium on Foundations of Computer
Science, FOCS 2020, Durham, NC, USA, November 16-19, 2020, IEEE, 2020, pp. 601–612.
Derek G Corneil, Helmut Lerchs, and L Stewart Burlingham, Complement reducible graphs,
Discrete Applied Mathematics 3 (1981), no. 3, 163–174.
Z. Dvořák, Constant-factor approximation of domination number in sparse graphs, European
J. Combin. 34 (2013), no. 5, 833–840.
Zdenek Dvorák and Sergey Norin, Strongly sublinear separators and polynomial expansion,
SIAM Journal on Discrete Mathematics 30 (2016), no. 2, 1095–1101.
M. Grohe, S. Kreutzer, R. Rabinovich, S. Siebertz, and K. Stavropoulos, Colouring and
covering nowhere dense graphs, SIAM J. Discrete Math. 32 (2018), no. 4, 2467–2481.
Sylvain Guillemot and Dániel Marx, Finding small patterns in permutations in linear time,
Proceedings of the Twenty-Fifth Annual ACM-SIAM Symposium on Discrete Algorithms,
SODA 2014, Portland, Oregon, USA, January 5-7, 2014, 2014, pp. 82–101.
H.A. Kierstead and D. Yang, Orderings on graphs and game coloring number, Order 20 (2003),
255–264.
J. Nešetřil and P. Ossona de Mendez, Sparsity (graphs, structures, and algorithms), Algorithms
and Combinatorics, vol. 28, Springer, 2012, 465 pages.
X. Zhu, Colouring graphs with bounded generalized colouring number, Discrete Math. 309
(2009), no. 18, 5562–5568.

11

